#include "I2Cdev.h"
#include <Servo.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>
#include "RF24.h"
#include "MPU6050_6Axis_MotionApps20.h"
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif
//we need this to see nrf24l01 configuration in serial
#include "printf.h"
#define PRESSURE_COUNTER 10


//end of libraries ###############################################

MPU6050 mpu(MPU6050_ADDRESS_AD0_HIGH);

//@@Altimeter module initialization
Adafruit_BMP280 bmp; // use I2C interface
Adafruit_Sensor *bmp_temp = bmp.getTemperatureSensor();
Adafruit_Sensor *bmp_pressure = bmp.getPressureSensor();
float m_initPressureValue = 0.0f;
float m_Altitude = 0.0f;
float a_kp = 0, a_ki = 0, a_kd = 0;
int max_a_pid = 100;
float a_p_out, a_i_out, a_d_out, a_output;
float a_now, a_lasttime;
float a_input, a_lastinput = 0, a_setpoint = 0;
float a_error, a_errorsum = 0, a_d_error, a_lasterror;
bool isAltitudeHolding = false;
//@@

float x_rotation, y_rotation, z_rotation;

float x_rotation_last = 0, y_rotation_last = 0,z_rotation_last = 0; 
bool isFirstData = true;
Servo motor1;
Servo motor2;
Servo motor3;
Servo motor4;
bool first_loop = true;
//radio
RF24 radio(9,10);
uint8_t data[7];
const uint64_t pipe = 0xE8E8F0F0E1LL;
long last_received;
int controll_number = 159;

//values = 5.2, 0.02, 1500
//variables for movement and positions ###########################################
//for my quadcopter this are the best settings for pid
//float x_kp = 2.15, x_ki = 0.02, x_kd = 70.0; //values for PID X axis
//float x_kp = 1.3, x_ki = 0, x_kd = 0; //values for PID X axis
float x_kp = 1.3, x_ki = 0.02, x_kd = 40.0; //values for PID X axis
int max_pid = 400;
float x_p_out, x_i_out, x_d_out, x_output; //outputs for PID
float x_now, x_lasttime = 0, x_timechange;
float x_input, x_lastinput = 0, x_setpoint = 0;
float x_error, x_errorsum = 0, x_d_error, x_lasterror;
//+ 0.4    +15
float y_kp = x_kp , y_ki = x_ki, y_kd = x_kd;
float y_p_out, y_i_out, y_d_out, y_output; //outputs for PID
float y_now, y_lasttime = 0, y_timechange;
float y_input, y_lastinput = 0, y_setpoint = 0;
float y_error, y_errorsum = 0, y_d_error, y_lasterror;

float z_kp = 2.2, z_ki = 0, z_kd = 10; //values for PID Z axis
float z_p_out, z_i_out, z_d_out, z_output; //outputs for PID
float z_now, z_lasttime = 0, z_timechange;
float z_input, z_lastinput = 0, z_setpoint = 0;
float z_error, z_errorsum = 0, z_d_error, z_lasterror;


//set it to 0 and see on serial port what is the value for x and y rotation, use only if your flightcontroller board is not perfevtly leveled. If your board is perfectly leveled set it to 0
float x_level_error = 0;
float y_level_error = 0;


/*
 *
 *
 * JUNE 2016 - APRIL 2017
 * C by Nikodem Bartnik
 * nikodem.bartnik@gmail.com
 * nikodembartnik.pl
 *
 *
 *
 */

#define INTERRUPT_PIN 2  // use pin 2 on Arduino Uno & most boards
int motor1_power;
int motor2_power;
int motor3_power;
int motor4_power;

float allmotors_power = 600;


// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container

VectorFloat gravity;    // [x, y, z]            gravity vector

float rotation[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
int safe_lock = 1;




volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}


void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    Wire.begin();
    Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
    Fastwire::setup(400, true);
#endif

    printf_begin();

    Serial.begin(115200);

//    motor1.attach(5, 1000, 2000);
//    motor2.attach(6, 1000, 2000);
//    motor3.attach(3, 1000, 2000);
//    motor4.attach(4, 1000, 2000);
    motor1.attach(5);
    motor2.attach(6);
    motor3.attach(3);
    motor4.attach(4);
    delay(3000);
    motor1.writeMicroseconds(1000);
    motor2.writeMicroseconds(1000);
    motor3.writeMicroseconds(1000);
    motor4.writeMicroseconds(1000);

        //-----------------------------------BMP_280_code_block--------------------------------------------
    Serial.println(F("BMP280 Sensor event test"));
    if (!bmp.begin(BMP280_ADDRESS_ALT, BMP280_CHIPID)) {
      Serial.println(F("Could not find a valid BMP280 sensor, check wiring or "
                      "try a different address!"));
      while (1) delay(10);                                    //if you can't find the sensor it will be stopped the programm
    }
  
        /* Default settings from datasheet. */
    bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                    Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                    Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                    Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                    Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
  
    bmp_temp->printSensorDetails();
    int getPressureCounter = PRESSURE_COUNTER;
//    Serial.print("Altimeter calibration");    // ground pressure callibration for best result 
//    while (getPressureCounter > 0)
//    {
//      m_initPressureValue += bmp.readPressure() / 100;
//      getPressureCounter--;
//      Serial.print(".");
//      delay(1000);
//    }
//    Serial.println();
//
//    m_initPressureValue /= PRESSURE_COUNTER;
    //-------------------------------------------------------------------------------------------

    Serial.println("Initializing I2C devices...");
    mpu.initialize();
    pinMode(INTERRUPT_PIN, INPUT);

    // verify connection
    Serial.println("Testing device connections...");
    Serial.println(mpu.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");
    //    bmp.begin();
    radio.begin();
    delay(1000);
    radio.setChannel(101);
    radio.setDataRate(RF24_1MBPS);
    radio.setPALevel(RF24_PA_MAX);

    radio.openReadingPipe(1,pipe);
    radio.startListening();

    // load and configure the DMP
    Serial.println("Initializing DMP...");
    devStatus = mpu.dmpInitialize();

    // gyro offsets here
    mpu.setXGyroOffset(170);
    mpu.setYGyroOffset(56);
    mpu.setZGyroOffset(-156);
    mpu.setZAccelOffset(2105);
    mpu.setYAccelOffset(-979);
    mpu.setXAccelOffset(-2953);

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {

        Serial.println("Enabling DMP...");
        mpu.setDMPEnabled(true);

        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();


        dmpReady = true;

        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print("DMP Initialization failed (code ");
        Serial.print(devStatus);
        Serial.println(")");
    }

    pinMode(A0, INPUT);
    pinMode(A1, INPUT);
    digitalWrite(A0, LOW);
    radio.printDetails();

}




void loop() {
  if(isAltitudeHolding)
  {
   // m_Altitude = bmp.readAltitude(m_initPressureValue);       //change on it after testing
   m_Altitude = bmp.readAltitude();
    Serial.print(F("Altitude: "));
    Serial.print(m_Altitude);
    Serial.println(" m"); 
  }

    if (radio.available()) {

        bool done = false;
        while (!done){

            done = radio.read(data, sizeof(data));

            // Serial.print("Controll number: ");
            //Serial.println(data[0]);

            if((millis()-last_received) < 3000){
                if(data[0] == controll_number){
                    //          Serial.print("DATA1: ");
                    //          Serial.println(data[1]);
//                    allmotors_power = map(data[1], 0, 400, 1000, 1600);
                    allmotors_power = map(data[1], 0, 255, 1000, 1400);
                    if(allmotors_power < 0){
                        allmotors_power = 0;
                    }


                    //allmotors_power = map(data[1], 0, 255, 800, 1600);
                    x_setpoint = data[2] - 20;
                    y_setpoint = data[3] - 20;
                    if(data[6] && data[6] != isAltitudeHolding)
                      clearAltitudeData();
                  
                    isAltitudeHolding = data[6];
                    //     safe_lock = data[5];
//                    Serial.print("GYRO X: ");
//                    Serial.print(x_rotation);
//                    Serial.print(" GYRO Y: ");
//                    Serial.print(y_rotation);
//                    Serial.print("GYRO Z: ");
//                    Serial.print(z_rotation);
                    //     Serial.print(" TIME: ");
                    //     Serial.println(millis());
                }
            }
            // Serial.println(data[1]);
            if(done == true){
                last_received = millis();
            }
        }
    }

    if((millis()-last_received) > 3000 && allmotors_power > 0){
        safe_lock = 0;
    }

    // if programming failed, don't try to do anything
    // if (!dmpReady) return;


    while (!mpuInterrupt && fifoCount < packetSize) {

    }

    // reset interrupt flag and get INT_STATUS byte
    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    // get current FIFO count
    fifoCount = mpu.getFIFOCount();

    // check for overflow (this should never happen unless our code is too inefficient)
    if ((mpuIntStatus & 0x10) || fifoCount == 1024) {


        // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } 
    else if (mpuIntStatus & 0x02) {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;

        if(safe_lock == 1){

            mpu.dmpGetQuaternion(&q, fifoBuffer);
            mpu.dmpGetGravity(&gravity, &q);
            mpu.dmpGetYawPitchRoll(rotation, &q, &gravity);

            x_rotation = rotation[1] * 180/M_PI - x_level_error;
            y_rotation = rotation[2] * 180/M_PI - y_level_error;
            z_rotation = rotation[0] * 180/M_PI;

            /*
          if(pressure_loop_number == 10){
           // Serial.print("Preasure: ");
            //Serial.println(bmp.readAltitude());
            pressure_loop_number = 0;
            allmotors_power = 1000;
           }
            pressure_loop_number++;
*/

            if(first_loop == true){
                z_setpoint = z_rotation;
                //  current_altitude = bmp.readAltitude();
                //set_altitude = current_altitude;
                first_loop = false;
            }

            motor1_power = allmotors_power;
            motor2_power = allmotors_power;
            motor3_power = allmotors_power;
            motor4_power = allmotors_power;
            
            if(allmotors_power > 1500)
                allmotors_power = 1500;  
            


            //----------------------------------------experimental block----------------------------
            if(isFirstData)
            {
                x_rotation_last = x_rotation;
                y_rotation_last = y_rotation;
            }

            if(abs(abs(x_rotation) - abs(x_rotation_last)) > 10)
                x_rotation = x_rotation_last;
            else
                x_rotation_last = x_rotation;

            if(abs(abs(y_rotation) - abs(y_rotation_last)) > 10)
                y_rotation = y_rotation_last;
            else
                y_rotation_last = y_rotation;

            //------------------------------------------------------------------------------------------

            if(isAltitudeHolding)
              a_output = calculatePID(3, m_Altitude);
          
            x_output = calculatePID(0, x_rotation);
            y_output = calculatePID(1, y_rotation);
            z_output = calculatePID(2, z_rotation);

//            Serial.print(" PID OUT X: ");
//            Serial.print(x_output);
//            Serial.print(" PID OUT Y: ");
//            Serial.print(y_output);
//            Serial.print("Z NOW: ");
//            Serial.print(z_output);

//            Serial.print(" SETPOINT ");
//            Serial.println(x_setpoint);

            //
            //  3     /|\     2
            //         |
            //         |
            //  1             4
            //
            //  Motor 1 - BL
            //  Motor 2 - UR
            //  Motor 3 - UL
            //  Motor 4 - BR

            motor1_power = motor1_power + y_output + x_output + z_output + a_output;
            motor4_power = motor4_power + y_output - x_output - z_output + a_output;
            motor2_power = motor2_power - y_output - x_output + z_output + a_output;
            motor3_power = motor3_power - y_output + x_output - z_output + a_output;
            
           if(motor1_power > 2000) motor1_power = 2000;
           if(motor2_power > 2000) motor2_power = 2000;
           if(motor3_power > 2000) motor3_power = 2000;
           if(motor4_power > 2000) motor4_power = 2000;

           //WARNING. AFTER POWER ON MOTORS WIIL BE ENABLED!!!
           if(motor1_power < 1100) motor1_power = 1100;
           if(motor2_power < 1100) motor2_power = 1100;
           if(motor3_power < 1100) motor3_power = 1100;
           if(motor4_power < 1100) motor4_power = 1100;
            
            motor1.writeMicroseconds(motor1_power);
            motor4.writeMicroseconds(motor4_power);
            motor2.writeMicroseconds(motor2_power);
            motor3.writeMicroseconds(motor3_power);
            mpu.resetFIFO();
        }
        else
        {
            motor1.write(0);
            motor2.write(0);
            motor3.write(0);
            motor4.write(0);
        }
    }
}

//
float calculatePID(int _axis, float _angel){

      // X AXIS
      if(_axis == 0){
                 x_now = millis();
                 x_timechange = x_now - x_lasttime;
                 x_error = x_setpoint - _angel;
                 x_p_out = (x_kp * x_error);
                 x_i_out += x_ki * x_error;
                
                 if(x_i_out > max_pid) x_i_out = max_pid;
                 else if( x_i_out < -max_pid) x_i_out = -max_pid;
                 
                 //x_d_error = (x_error - x_lasterror) / x_timechange;
                 //or
                 x_d_error = (x_error - x_lasterror);
                 x_d_out = x_kd * x_d_error;
                 x_lasterror = x_error;
                 x_output = x_p_out + x_i_out + x_d_out;
                 if(x_output > max_pid)         x_output = max_pid;
                 else if(x_output < -(max_pid)) x_output = -(max_pid);
                 x_lasttime = millis();
                 return x_output;
      }

      // Y AXIS
      else if(_axis == 1){
                 y_now = millis();
                 y_timechange = y_now - y_lasttime;
                 y_error = y_setpoint - _angel;
                 y_p_out = (y_kp * y_error);
                 y_i_out += y_ki * y_error;

                 if(y_i_out > max_pid) y_i_out = max_pid;
                 else if( y_i_out < -max_pid) y_i_out = -max_pid;

//                 y_d_error = (y_error - y_lasterror) / y_timechange;
                 //or
                 y_d_error = (y_error - y_lasterror);
                 y_d_out = y_kd * y_d_error;
                 y_lasterror = y_error;
                 y_output = y_p_out + y_i_out + y_d_out;
                 if(y_output > max_pid)         y_output = max_pid;
                 else if(y_output < -(max_pid)) y_output = -(max_pid);
                 y_lasttime = millis();
                 return y_output;

                 // ALTITUDE
     // } else if(_axis == 2){
      //           return (set_altitude - current_altitude) * 20;
      }else if(_axis == 2){
                 z_now = millis();
                 z_timechange = z_now - z_lasttime;
                 z_error = z_setpoint - _angel;
                 z_p_out = (z_kp * z_error);
                 z_i_out += z_ki * z_error;

                 if(z_i_out > max_pid) z_i_out = max_pid;
                 else if(z_i_out < -max_pid) z_i_out = -max_pid;

                 z_d_error = (z_error - z_lasterror);
                 z_d_out = z_kd * z_d_error;
                 z_lasterror = z_error;
                 z_output = z_p_out + z_i_out + z_d_out;
                 if(z_output > max_pid) z_output = max_pid;
                 else if(z_output < -max_pid) z_output = -max_pid;
                 z_lasttime = millis();
                 return z_output;

                 // ALTITUDE
      }
      else if(_axis == 3)
      {
          a_error = a_setpoint - _angel;
          a_p_out = a_kp * a_error;
          a_i_out += a_ki * a_error;

          if(a_i_out > max_a_pid) a_i_out = max_a_pid;
          else if(a_i_out < max_a_pid) a_i_out = -max_a_pid;

          a_d_error = (a_error - a_lasterror);
          a_d_out = a_kd * a_d_error;
          a_lasterror = a_error;
          a_output = a_p_out + a_i_out + a_d_out;

          if(a_output > max_a_pid) a_output = max_a_pid;
          else if(a_output < max_a_pid) a_output = -max_a_pid;
          return a_output;
      }
      else
      {
        return 0;
      }
}

void clearAltitudeData()
{
  a_p_out = 0;
  a_i_out = 0;
  a_d_out = 0;

  a_lasterror = 0;
  a_setpoint = m_Altitude;
  
}
